# Log in log out 

**Features**

- Gebruiksvriendelijk inlogformulier dat om een gebruikersnaam en wachtwoord vraagt.
- Foutmelding wordt weergegeven als de ingevoerde inloggegevens onjuist zijn.
- Gebruikers worden naar index.php doorgestuurd bij juiste inloggegevens.
- Toegang tot beveiligde pagina's wordt geweigerd na uitloggen.
- Uitloggen is eenvoudig en veilig door het verwijderen van de sessie.

## Hoe een PHP-server te starten met XAMPP

### 1. Installeer XAMPP

- Download en installeer XAMPP vanaf [de XAMPP-website](https://www.apachefriends.org/index.html).

### 2. Start XAMPP

- Open het XAMPP-controlepaneel via het startmenu of een vergelijkbare methode op jouw besturingssysteem.

### 3. Start Apache

- Zoek in het XAMPP-controlepaneel naar "Apache" en klik op de knop "Start" om de Apache-webserver te starten.

### 4. Plaats bestanden in de "htdocs"-map

- Plaats je PHP-bestanden in de "htdocs"-map van XAMPP. Bijvoorbeeld: `C:\xampp\htdocs` op Windows.

### 5. Toegang tot je PHP-toepassing

- Open je webbrowser en ga naar `http://localhost/jouw_bestandsnaam.php`, waarbij je "jouw_bestandsnaam.php" vervangt door de daadwerkelijke naam van je PHP-bestand.  