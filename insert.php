<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Add pagina</title>
</head>
<body>
    <?php 
    include 'database_connectie.php';
    ?>

    <h1>Nieuwe media</h1>
    <a class="go_back" href="index.php">Go back</a>

    <form method="POST">
        <label for="titel">
            <span>Titel</span>
            <input type="text" id="titel" name="titel" value="">
        </label><br>
        
        <label for="rating">
            <span>Rating</span>
            <input type="text" id="rating" name="rating" value="">
        </label><br>

        <label for="omschrijving">
            <span>Omschrijving</span>
            <input type="text" id="omschrijving" name="omschrijving" value="">
        </label><br>

        <label for="aantal_awards">
            <span>Aantal awards</span>
            <input type="text" id="aantal_awards" name="aantal_awards" value="">
        </label><br>

        <label for="lengte_minuten">
            <span>Lengte minuten</span>
            <input type="text" id="lengte_minuten" name="lengte_minuten" value="">
        </label><br>

        <label for="datum">
            <span>Datum</span>
            <input type="date" id="datum" name="datum" value="">
        </label><br>

        <label for="sezionen">
            <span>Sezionen</span>
            <input type="text" id="sezionen" name="sezionen" value="">
        </label><br>

        <label for="land">
            <span>Land</span>
            <input type="text" id="land" name="land" value="">
        </label><br>

        <label for="id_trailer">
            <span>Id trailer</span>
            <input type="text" id="id_trailer" name="id_trailer" value="">
        </label><br>

        <label for="type_media">
            <span>Type media</span>
            <select name="type_media" id="type_media">
                <option value="series">Series</option>
                <option value="films">Films</option>
            </select>
        </label><br>

        <button type="submit" class="btn">Add</button>
        
    </form>

    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $data = [
            't' => $_POST['titel'],
            'r' => $_POST['rating'],
            'o' => $_POST['omschrijving'],
            'a' => $_POST['aantal_awards'],
            'l' => $_POST['lengte_minuten'],
            'd' => $_POST['datum'],
            's' => $_POST['sezionen'],
            'land' => $_POST['land'],
            'id' => $_POST['id_trailer'],
            'type_m' => $_POST['type_media'],
        ];
    
        $sql = "INSERT INTO media (titel, rating, omschrijving, aantal_awards, lengte_minuten, datum, sezionen, land, id_trailer, type_media) VALUES (:t, :r, :o, :a, :l, :d, :s, :land, :id, :type_m)";
    
        $stmt = $pdo->prepare($sql);
        $stmt->execute($data);
    
        header("Location: index.php");
        exit();
    }
    
    ?>
</body>
</html>