DROP DATABASE IF EXISTS `netland`;

CREATE DATABASE `netland`;

USE `netland`;

CREATE TABLE series (
	id MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    rating DECIMAL(2,1) NULL,
    summary TEXT NOT NULL,
    has_won_awards INT NOT NULL,
    seasons INT NOT NULL,
    country VARCHAR(5) NOT NULL,
    spoken_in_language ENUM('NL', 'EN') NOT NULL
);

INSERT INTO series (title, rating, summary, has_won_awards, seasons, country, spoken_in_language)  
VALUES
	("The good place", 4.5, "De serie gaat over een vrouw, Eleanor Shellstrop, die zich in het hiernamaals bevindt. Ze wordt verwelkomd door Michael, de ""architect"" van het utopische dorpje waar ze voor eeuwig gaat wonen. Er zijn twee delen in het hiernamaals, The Good Place (""goede plek"") en The Bad Place (""slechte plek""); welke wordt bepaald door ethische punten voor elke handeling op aarde.", 0, 4, "UK", "EN"),
    ("Game of Thrones", 5.0, "Op het continent Westeros regeert koning Robert Baratheon al meer dan zeventien jaar lang over de Zeven Koninkrijken, na zijn opstand tegen koning Aerys II Targaryen ""De Krankzinnige"". Als zijn adviseur Jon Arryn wordt vermoord, vraagt hij zijn oude vriend Eddard Stark, de Heer van Winterfell en Landvoogd van het Noorden, om zijn plaats in te nemen. Eddard gaat met tegenzin akkoord, en trekt met zijn twee dochters, Sansa en Arya (Maisie Williams), naar de hoofdstad in het zuiden. Vlak voor hun vertrek wordt een van zijn jongste zonen, Bran Stark, uit een van de torens van Winterfell geduwd, na getuige te zijn geweest van de incestueuze affaire tussen koningin Cersei en haar tweelingbroer, Jaime Lannister.", 1, 7, "UK", "EN"),
    ("Breaking Bad", 2.0, "Walter White is in 2008 een overgekwalificeerde scheikundeleraar op een middelbare school in Albuquerque. Op het moment dat zijn vrouw onverwacht zwanger is van hun tweede kind, stort Walters wereld in. De dokter heeft vastgesteld dat hij terminaal ziek is. Walter heeft longkanker en lijkt niet lang meer te zullen leven. Om ervoor te zorgen dat zijn gezin na zijn dood niet in een financiële crisis belandt en tevens om zijn eigen behandelingen te betalen, besluit Walter over te schakelen op een leven als misdadiger. Met de hulp van Jesse Pinkman, een uitgevallen leerling die hij nog scheikunde gegeven heeft, maakt en verkoopt hij de drug crystal meth. Terwijl hij doorgaat met lesgeven, komt het belang van scheikunde in de moderne maatschappij prangend in zijn lessen naar voren. Zijn product is meer dan 99% zuiver en dit feit loopt als een rode draad door de hele serie heen.", 1, 3, "UK", "EN"),
    ("Penoza", 3.2, "Hoofdrolspeelster Carmen van Walraven (Monic Hendrickx) ontdekt dat haar man Frans (Thomas Acda) een veel belangrijker rol in de onderwereld speelt dan ze dacht. Ze dwingt hem dan ook ermee te stoppen. Net wanneer alles weer goed lijkt te gaan, wordt haar man voor de ogen van hun jongste zoon Boris (Stijn Taverne) geliquideerd. Carmen krijgt last van schuldeisers en bedreigingen. Ook justitie zit achter haar aan omdat die wil dat zij gaat getuigen tegen de compagnons van haar man.Carmen wil niet als beschermd getuige door het leven gaan en kiest voor een moeilijk alternatief: ze werkt zich naar de top van de georganiseerde misdaad, waar niemand nog aan haar of haar gezin durft te komen. In het vervolg daarop weet ze al snel niet meer wie ze moet vertrouwen, en worden de grenzen tussen goed en kwaad steeds onduidelijker.", 0, 3, "NL", "NL"),
    ("De luizenmoeder", 4.8, "Het verhaal speelt zich af op de fictieve basisschool De Klimop in Dokkum. De school heeft een zwaar jaar achter de rug, waarin enkele leraren en de conciërge ontslagen zijn. Het is nu aan de schoolleiding om in het nieuwe schooljaar een frisse start te maken. Centraal staan Hannah (Jennifer Hoffman), de moeder van Floor, de ""luizenmoeder"", en juf Ank (Ilse Warringa), de kordate onderwijzeres. Als moeder van een nieuwe leerling moet Hannah zich staande houden in een absurdistische wereld van hangouders, moedermaffia, schoolpleinregels, rigide verjaardagsprotocollen, verantwoorde traktaties, parkeerbeleid, appgroepjes, ouderparticipatie en ander leed. Ook worden de belevenissen van de andere ouders en de schoolleiding gevolgd. De ouders (moeders) worden geacht het onderwijs te ondersteunen als vrijwilligers en de onderste tree in de bijbehorende hiërarchie die tot de ouderraad loopt is die van luizenmoeder, de moeder die schoolkinderen met een luizenkam controleert op luizen in het haar en deze verwijdert.", 1, 2, "NL", "NL"),
    ("My little pony", 1, "De serie begint met een eenhoorn genaamd Twilight Sparkle, een student van Equestria`s heerser, prinses Celestia. Nadat ze ziet hoe haar student zich alleen maar bezighoudt met boeken, stuurt prinses Celestia haar naar Ponyville met de opdracht een paar vrienden te maken. Twilight Sparkle, samen met haar assistent, een babydraak genaamd Spike, raakt bevriend met de pony`s Pinkie Pie, Applejack, Rainbow Dash, Rarity en Fluttershy. Samen beleven ze avonturen binnen en buiten de stad en lossen ze diverse problemen op. De meeste afleveringen eindigen met Twilight Sparkle of iemand anders die een brief schrijft aan de prinses over wat ze die aflevering geleerd heeft over de magie van de vriendschap. Ook zit er in iedere aflevering een belangrijke les over vriendschap.", 0, 25, "UK", "NL");

CREATE TABLE films (
	id MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    titel VARCHAR(100) NOT NULL,
    duur INT NOT NULL,
    datum_van_uitkomst DATE,
	land_van_uitkomst VARCHAR(50),
    omschrijving VARCHAR (255) NOT NULL,
    id_van_de_trailer VARCHAR (100) NOT NULL    
);

INSERT INTO films (titel, duur, datum_van_uitkomst, land_van_uitkomst, omschrijving, id_van_de_trailer)
VALUES 
("Leave the World Behind", 138, "2023-10-25", "United States", "A family`s getaway to a luxurious rental home takes an ominous turn when a cyberattack knocks out their devices, and two strangers appear at their door.", "cMVBi_e8o-Y&ab_channel=Netflix"),
("Animal", 204, "2023-11-30", "India", "A father, who is often away due to work, is unable to comprehend the intensity of his son`s love. Ironically, this fervent love and admiration for his father and family creates conflict between the father and son.", "8FkLRUJj-o0&ab_channel=T-Series"),
("Wonka", 116, "2023-11-28", "United States, United Kingdom, Canada", "With dreams of opening a shop in a city renowned for its chocolate, a young and poor Willy Wonka discovers that the industry is run by a cartel of greedy chocolatiers.", "otNh9bTjXWg&ab_channel=WarnerBros.Pictures"),
("Poor Things", 141, "2023-09-01", "Ireland, United Kingdom, United States", "The incredible tale about the fantastical evolution of Bella Baxter; a young woman brought back to life by the brilliant and unorthodox scientist, Dr. Godwin Baxter.", "0JdlYZ8vPkA&ab_channel=MovieTrailersSource"), 
("Godzilla Minus One", 124, "2023-11-01", "Japan", "Post war Japan is at its lowest point when a new crisis emerges in the form of a giant monster, baptized in the horrific power of the atomic bomb.", "VvSrHIX5a-0&ab_channel=GODZILLAOFFICIALbyTOHO");


CREATE TABLE media (
	id MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    titel VARCHAR(100) NOT NULL,
    rating DECIMAL(2,1) NULL,
    omschrijving TEXT NOT NULL,
    aantal_awards INT NOT NULL,
    lengte_minuten INT NOT NULL,
    datum date NOT NULL,
    sezionen INT NOT NULL,
    land VARCHAR(100) NOT NULL,
    id_trailer VARCHAR(100) NOT NULL,
    type_media ENUM('series', 'films') NOT NULL    
);


INSERT INTO media (titel, rating, omschrijving, aantal_awards, lengte_minuten, datum, sezionen, land, id_trailer, type_media)  
VALUES
	("The good place", 4.5, "De serie gaat over een vrouw, Eleanor Shellstrop, die zich in het hiernamaals bevindt. Ze wordt verwelkomd door Michael, de ""architect"" van het utopische dorpje waar ze voor eeuwig gaat wonen. Er zijn twee delen in het hiernamaals, The Good Place (""goede plek"") en The Bad Place (""slechte plek""); welke wordt bepaald door ethische punten voor elke handeling op aarde.", 0, 42, "2021-05-26", 4, "UK", "", "series"),
    ("Game of Thrones", 5.0, "Op het continent Westeros regeert koning Robert Baratheon al meer dan zeventien jaar lang over de Zeven Koninkrijken, na zijn opstand tegen koning Aerys II Targaryen ""De Krankzinnige"". Als zijn adviseur Jon Arryn wordt vermoord, vraagt hij zijn oude vriend Eddard Stark, de Heer van Winterfell en Landvoogd van het Noorden, om zijn plaats in te nemen. Eddard gaat met tegenzin akkoord, en trekt met zijn twee dochters, Sansa en Arya (Maisie Williams), naar de hoofdstad in het zuiden. Vlak voor hun vertrek wordt een van zijn jongste zonen, Bran Stark, uit een van de torens van Winterfell geduwd, na getuige te zijn geweest van de incestueuze affaire tussen koningin Cersei en haar tweelingbroer, Jaime Lannister.", 1, 59, "2016-01-16", 7, "UK", "", "series"),
    ("Breaking Bad", 2.0, "Walter White is in 2008 een overgekwalificeerde scheikundeleraar op een middelbare school in Albuquerque. Op het moment dat zijn vrouw onverwacht zwanger is van hun tweede kind, stort Walters wereld in. De dokter heeft vastgesteld dat hij terminaal ziek is. Walter heeft longkanker en lijkt niet lang meer te zullen leven. Om ervoor te zorgen dat zijn gezin na zijn dood niet in een financiële crisis belandt en tevens om zijn eigen behandelingen te betalen, besluit Walter over te schakelen op een leven als misdadiger. Met de hulp van Jesse Pinkman, een uitgevallen leerling die hij nog scheikunde gegeven heeft, maakt en verkoopt hij de drug crystal meth. Terwijl hij doorgaat met lesgeven, komt het belang van scheikunde in de moderne maatschappij prangend in zijn lessen naar voren. Zijn product is meer dan 99% zuiver en dit feit loopt als een rode draad door de hele serie heen.", 1, 38, "2014-02-08", 3, "UK", "", "series"),
    ("Penoza", 3.2, "Hoofdrolspeelster Carmen van Walraven (Monic Hendrickx) ontdekt dat haar man Frans (Thomas Acda) een veel belangrijker rol in de onderwereld speelt dan ze dacht. Ze dwingt hem dan ook ermee te stoppen. Net wanneer alles weer goed lijkt te gaan, wordt haar man voor de ogen van hun jongste zoon Boris (Stijn Taverne) geliquideerd. Carmen krijgt last van schuldeisers en bedreigingen. Ook justitie zit achter haar aan omdat die wil dat zij gaat getuigen tegen de compagnons van haar man.Carmen wil niet als beschermd getuige door het leven gaan en kiest voor een moeilijk alternatief: ze werkt zich naar de top van de georganiseerde misdaad, waar niemand nog aan haar of haar gezin durft te komen. In het vervolg daarop weet ze al snel niet meer wie ze moet vertrouwen, en worden de grenzen tussen goed en kwaad steeds onduidelijker.", 0, 102, "2002-09-28", 3, "NL", "", "series"),
    ("De luizenmoeder", 4.8, "Het verhaal speelt zich af op de fictieve basisschool De Klimop in Dokkum. De school heeft een zwaar jaar achter de rug, waarin enkele leraren en de conciërge ontslagen zijn. Het is nu aan de schoolleiding om in het nieuwe schooljaar een frisse start te maken. Centraal staan Hannah (Jennifer Hoffman), de moeder van Floor, de ""luizenmoeder"", en juf Ank (Ilse Warringa), de kordate onderwijzeres. Als moeder van een nieuwe leerling moet Hannah zich staande houden in een absurdistische wereld van hangouders, moedermaffia, schoolpleinregels, rigide verjaardagsprotocollen, verantwoorde traktaties, parkeerbeleid, appgroepjes, ouderparticipatie en ander leed. Ook worden de belevenissen van de andere ouders en de schoolleiding gevolgd. De ouders (moeders) worden geacht het onderwijs te ondersteunen als vrijwilligers en de onderste tree in de bijbehorende hiërarchie die tot de ouderraad loopt is die van luizenmoeder, de moeder die schoolkinderen met een luizenkam controleert op luizen in het haar en deze verwijdert.", 1, 66, "2018-04-25", 2, "NL", "", "series"),
    ("My little pony", 1, "De serie begint met een eenhoorn genaamd Twilight Sparkle, een student van Equestria`s heerser, prinses Celestia. Nadat ze ziet hoe haar student zich alleen maar bezighoudt met boeken, stuurt prinses Celestia haar naar Ponyville met de opdracht een paar vrienden te maken. Twilight Sparkle, samen met haar assistent, een babydraak genaamd Spike, raakt bevriend met de pony`s Pinkie Pie, Applejack, Rainbow Dash, Rarity en Fluttershy. Samen beleven ze avonturen binnen en buiten de stad en lossen ze diverse problemen op. De meeste afleveringen eindigen met Twilight Sparkle of iemand anders die een brief schrijft aan de prinses over wat ze die aflevering geleerd heeft over de magie van de vriendschap. Ook zit er in iedere aflevering een belangrijke les over vriendschap.", 0, 52, "2022-12-22", 25, "UK", "", "series"),
    ("Leave the World Behind", 2.5, "A family`s getaway to a luxurious rental home takes an ominous turn when a cyberattack knocks out their devices, and two strangers appear at their door.", 0, 138, "2023-10-25", 1, "United States", "cMVBi_e8o-Y&ab_channel=Netflix", "films"),
    ("Animal", 4.2, "A father, who is often away due to work, is unable to comprehend the intensity of his son`s love. Ironically, this fervent love and admiration for his father and family creates conflict between the father and son.", 1, 204, "2023-11-30", 2, "India", "8FkLRUJj-o0&ab_channel=T-Series", "films"),
    ("Wonka", 5.5, "With dreams of opening a shop in a city renowned for its chocolate, a young and poor Willy Wonka discovers that the industry is run by a cartel of greedy chocolatiers.", 2,  116, "2023-11-28", 3, "United States, United Kingdom, Canada",  "otNh9bTjXWg&ab_channel=WarnerBros.Pictures", "films"),
    ("Poor Things", 5.0, "The incredible tale about the fantastical evolution of Bella Baxter; a young woman brought back to life by the brilliant and unorthodox scientist, Dr. Godwin Baxter.", 3, 141, "2023-09-01", 2, "Ireland, United Kingdom, United States", "0JdlYZ8vPkA&ab_channel=MovieTrailersSource", "films"), 
    ("Godzilla Minus One", 4.8, "Post war Japan is at its lowest point when a new crisis emerges in the form of a giant monster, baptized in the horrific power of the atomic bomb.", 2, 124, "2023-11-01", 1, "Japan", "VvSrHIX5a-0&ab_channel=GODZILLAOFFICIALbyTOHO", "films");


CREATE TABLE gebruikers (
	id MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(100) NOT NULL,
    wachtwoord text NOT NULL  
);

INSERT INTO gebruikers (username, wachtwoord)
VALUES ('test-user', 'w@chtw00rd');