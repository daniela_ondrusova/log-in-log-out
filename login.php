<?php

session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Log in</title>
</head>
<body>
    <h1>Netland Admin Panel</h1>

    <?php
        include 'database_connectie.php';

        $row = $stmt->fetch();
        $id = $row['id'];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST['name'];
            $password = $_POST['password'];

        if ($name == "test-user" && $password == "w@chtw00rd") {
            if (!isset($_SESSION['loggedInUser'])) {
                $_SESSION['loggedInUser'] = $id;
            }
            header("location: index.php");
            exit();
        } else {
            echo "<h3 style='color: red'>Invalide gebruikersnaam/wachtwoord combinatie</h3>";
        }
    }
    ?>
    
    <form method="POST">
        <input type="text" name="name" placeholder="Username"><br><br>
        <input type="text" name="password" placeholder="Wachtwoord"><br><br>
        <button class="btn" type="submit">Log in</button>
    </form>
</body>
</html>