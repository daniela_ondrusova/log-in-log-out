<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Detail pagina</title>
</head>
<body>

    <?php 
        include 'database_connectie.php';
        $id = $_GET['id']; 
        $stmt1 = $pdo->query("SELECT * FROM media WHERE id = $id");
        $row = $stmt1->fetch();
    ?>

    <h1><?php echo $row['titel']; ?></h1>
    <a href="index.php">Go back</a>
    <table>
            <tr>
                <th>Information</th>
                <th>Information</th>
            </tr>
            <tr> 
                <td>Rating</td> 
                <td><?php echo $row['rating']; ?></td> 
            </tr>
            <tr> 
                <td>Awards</td>
                <td><?php 
                if ($row['aantal_awards'] > 0) {
                        echo "Ja";
                } else {
                        echo "Nee";
                }
                ?></td> 
            </tr> 
            <tr> 
                <td>Lengte in minuten</td> 
                <td><?php echo $row['lengte_minuten']; ?></td> 
            </tr>
            <tr> 
                <td>Datum</td> 
                <td><?php echo $row['datum']; ?></td> 
            </tr>
            <tr> 
                <td>Sezionen</td> 
                <td><?php echo $row['sezionen']; ?></td> 
            </tr>
            <tr> 
                <td>Land</td> 
                <td><?php echo $row['land']; ?></td> 
            </tr>
            <tr> 
                <td>ID trailer</td> 
                <td><?php echo $row['id_trailer']; ?></td> 
            </tr>
    </table>

    <h2>Beschrijving:</h2>
    <p><?php echo $row['omschrijving']; ?></p><br>
    <a class="btn" href="edit.php?id=<?php echo $id ?>">Edit</a>
    
</body>
</html>