<?php

session_start();
if (!isset($_SESSION['loggedInUser'])) {
    header("location: login.php");
    die;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>You're in or you're out</title>
</head>

<body>
    <?php include 'database_connectie.php'; ?>
    <?php

    $row = $stmt->fetch();
    $id = $row['id'];
    function getMedia($stmt, $type, $c) 
    {
        $stmt->execute();
        while ($row = $stmt->fetch()) {
            if ($row['type_media'] == $type) {
                $id = $row['id'];
                echo "
                <tr>
                    <td>" . $row['titel'] . "</td>
                    <td>" . $row[$c] . "</td>
                    <td><a href='detail.php?id=$id'>Bekijk details</a></td>
                </tr>";
            }
        }
        $stmt->closeCursor();
    }

    ?>
        <div>
            <h1>Welkom op het netland beheerders paneel</h1>
            <a href="logout.php" class="btn">Log out</a>
        </div>
        
        <h3>Series</h3>
        <a class="btn" href="insert.php?id=<?php echo $id + 1?>">Add new serie or film</a>
    <table>
            <tr>
                <th><a href="index.php?name=">Title</a></th>
                <th><a href="index.php?sorting=">Rating</a></th>
                <th>Details</th>
            </tr>
        
        <?php      
        getMedia($stmt, "series", "rating");
       
        ?>        
    </table>

    <h3>Films</h3>
    <table>
            <tr>
                <th><a href="index.php?name1=">Titel</a></th>
                <th><a href="index.php?sorting1=">Duur</a></th>
                <th>Details</th>
            </tr>
        
        <?php
            getMedia($stmt, "films", "lengte_minuten");
        ?>
    </table>
    
</body>
</html>