<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Edit pagina</title>
</head>
<body>
    <?php 
    include 'database_connectie.php';
    $id = $_GET['id'];
    $stmt1 = $pdo->query("SELECT * FROM media WHERE id = $id");
    $row = $stmt1->fetch(); 
    ?>

    <h1><?php echo $row['titel']; ?></h1>
    <a class="go_back" href="detail.php?id=<?php echo $id ?>">Go back</a>
    <form method="POST">
        
        <label for="title">
            <span>Title</span>
            <input type="text" id="titel" name="titel" value="<?php echo $row['titel']; ?>">
        </label><br>
        
        <label for="rating">
            <span>Rating</span>
            <input type="text" id="rating" name="rating" value="<?php echo $row['rating']; ?>">
        </label><br>

        <label for="awards">
            <span>Awards</span>
            <input type="text" id="awards" name="aantal_awards" value="<?php echo $row['aantal_awards']; ?>">
        </label><br>

        <label for="lengte">
            <span>Lengte minuten</span>
            <input type="text" id="lengte" name="lengte_minuten" value="<?php echo $row['lengte_minuten']; ?>">
        </label><br>

        <label for="datum">
            <span>Datum</span>
            <input type="text" id="datum" name="datum" value="<?php echo $row['datum']; ?>">
        </label><br>

        <label for="sezionen">
            <span>Sezionen</span>
            <input type="text" id="sezionen" name="sezionen" value="<?php echo $row['sezionen']; ?>">
        </label><br>

        <label for="land">
            <span>Land</span>
            <input type="text" id="land" name="land" value="<?php echo $row['land']; ?>">
        </label><br>

        <label for="id_trailer">
            <span>ID trailer</span>
            <input type="text" id="id_trailer" name="id_trailer" value="<?php echo $row['id_trailer']; ?>">
        </label><br>

        <label for="omschrijving">
            <span>Omschrijving</span>
            <input type="text" id="omschrijving" name="omschrijving" value="<?php echo $row['omschrijving']; ?>">
        </label><br>

        <button type="submit" class="btn">Send</button>
    </form>

    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach ($_POST as $column => $value) {
                $stmt1 = $pdo->prepare("UPDATE media SET $column = '$value' WHERE id = $id");
                $stmt1->execute();
        }
    
        header("Location: index.php");
        exit(); 
    }
    ?>
</body>
</html>